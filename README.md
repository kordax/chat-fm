# Chat file manager server readme

This application is built upon Spring Boot and Gradle.
Use gradle commands to build/run this application or use provided IntelliJ IDEA project.

> Please note that you have to configure your hosts file to include db-host and kafka-host entries.

## Requirements

1) Java >11;
2) Gradle >4.10.*.

## How to build
1. `gradle build -x test` or `gradle build` to run tests.

## How to run application

You have several options to run this application:
1. Pass java jar manually: `java -jar build/libs/chat-fm-$VERSION.jar` with application arguments using `-Darg=1`;
    * Do the same for dev profile with debug on: `java -Dspring.profiles.active=dev -jar build/libs/chat-fm-0.4.65.2.jar`.
2. `gradle bootRun` (not recommended).

# How it works
There are two URI REST mappings available:
1. `$SERVER_HOST:$SERVER_PORT/upload`. This mapped controller expects `POST` request and it's main objective is to
 upload new file to S3 storage.
    * Mandatory parameters: context, token.
2. `$SERVER_HOST:$SERVER_PORT/download`. This mapped controller expects `GET` request and it's main objective is to
    retrieve file using stored/actual ACL.
    * Mandatory parameters: fileName, token.

    
    
## Operations examples with CURL
### Upload content
```
curl -F 'token=sometoken32467' -F ‘payload=@path/to/local/file’ 192.168.2.3:8082/upload 
```

## Available query parameters
| Parameter name | Description                             |
|----------------|-----------------------------------------|
| context        | Current context that's used to form ACL |
| token          | JWT token                               |
| payload        | Content payload
| fileName       | Name of file to download                |
