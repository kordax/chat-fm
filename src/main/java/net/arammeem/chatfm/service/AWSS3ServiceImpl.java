/*
 * [AWSS3ServiceImpl.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import net.arammeem.chatfm.entity.rest.UploadResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.utils.Md5Utils;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

/**
 * AWS S3 policyManagementService implementation that uses AWS provided S3 configurable client.
 *
 */
@Service
public class AWSS3ServiceImpl implements S3Service {
    private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3ServiceImpl.class);

    @Value("${fm.s3.bucket}")
    private String bucket;
    @Value("${fm.s3.remote-location}")
    private String remoteLocation;
    @Value("${fm.s3.auth.aws-access-key}")
    private String awsAccessKey;
    @Value("${fm.s3.auth.aws-secret-access-key}")
    private String awsSecretAccessKey;
    @Value("${fm.s3.auth.aws-session-token}")
    private String awsSessionToken;
    @Value("${fm.s3.auth.owner-id}")
    private String ownerId;
    @Value("${fm.s3.auth.owner-display-name}")
    private String ownerDisplayName;
    @Value("${fm.s3.upload.chunk-size-bytes}")
    private int chunkSizeBytes;

    private final S3Client s3Client;
    private final FilePolicyManagementService policyManagementService;

    @Autowired
    public AWSS3ServiceImpl(final S3Client s3Client, final FilePolicyManagementService policyManagementService) {
        this.s3Client = s3Client;
        this.policyManagementService = policyManagementService;
    }

    /**
     * Upload file.
     * @see <a href="https://docs.aws.amazon.com/en_us/amazonglacier/latest/dev/api-upload-part.html">Upload Part (PUT uploadID)</a>.
     *
     * @param bytes bytes
     * @param contentType content type
     * @param fileName file name
     * @return HTTP response for client to send back
     */
    @Override
    public ResponseEntity<UploadResultEntity> uploadFile(final byte[] bytes, final String contentType, final String fileName) {
        final String requestUUID = UUID.randomUUID().toString();
        LOGGER.info("Received request to upload file [\"{}\", \"{}\"]", fileName, requestUUID);

        // For example, in the URL http://doc.s3.amazonaws.com/2006-03-01/AmazonS3.wsdl,
        // "doc" is the name of the bucket and "2006-03-01/AmazonS3.wsdl" is the key.
        final String key = remoteLocation + "/" + fileName;
        LOGGER.debug("Using credentials:\nAccess key: {}\nSecret access key: {}\n Session token: {}", awsAccessKey, awsSecretAccessKey, awsSessionToken);

        try {
            s3Client.putObject(PutObjectRequest.builder()
                        .acl(ObjectCannedACL.BUCKET_OWNER_READ)
                        .bucket(bucket)
                        .contentLength((long) bytes.length)
                        .contentType(contentType)
                        .key(key)
                        .metadata(policyManagementService.getPolicy(fileName))
                        .build(), RequestBody.fromBytes(bytes)
                    );
        } catch (SdkException e) {
            LOGGER.error("Failed to initialize upload for file [\"{}\", \"{}\"]", fileName, requestUUID, e);

            return new ResponseEntity<>(
                    new UploadResultEntity(
                            UploadResultEntity.Result.FAILURE,
                            fileName,
                            "Failed to initialize upload for file"
                    ),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }

        // Optional ACL request
        final PutObjectAclRequest aclRequest = PutObjectAclRequest.builder()
                .accessControlPolicy(AccessControlPolicy.builder()
                        .owner(Owner.builder()
                                .id(ownerId)
                                .displayName(ownerDisplayName)
                                .build()
                        ).build()
                ).acl(ObjectCannedACL.BUCKET_OWNER_READ)
                .bucket(bucket)
                .contentMD5(Md5Utils.md5AsBase64(bytes))
                .build();

        return new ResponseEntity<>(
                new UploadResultEntity(
                        UploadResultEntity.Result.FAILURE,
                        fileName,
                        "Unknown error has occurred"),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Upload file as multipart.
     * @see <a href="https://docs.aws.amazon.com/en_us/amazonglacier/latest/dev/api-upload-part.html">Upload Part (PUT uploadID)</a>
     *
     * @param bytes bytes
     * @param contentType content type
     * @param fileName file name
     * @return HTTP response for client to send back
     */
    @Override
    public ResponseEntity<UploadResultEntity> uploadFileAsMultipart(final byte[] bytes, final String contentType, final String fileName) {
        String requestUUID = UUID.randomUUID().toString();
        LOGGER.info("Received request to upload file [\"{}\", \"{}\"]", fileName, requestUUID);
        LOGGER.debug("Using credentials:\nAccess key: {}\nSecret access key: {}\nSession token: {}", awsAccessKey, awsSecretAccessKey, awsSessionToken);

        // Optional ACL request
        final PutObjectAclRequest aclRequest = PutObjectAclRequest.builder()
                .accessControlPolicy(AccessControlPolicy.builder()
                        .owner(Owner.builder()
                                .id(ownerId)
                                .displayName(ownerDisplayName)
                                .build()
                        ).build()
                ).acl(ObjectCannedACL.BUCKET_OWNER_READ)
                .bucket(bucket)
                .contentMD5(Md5Utils.md5AsBase64(bytes))
                .build();

        try {
            CreateMultipartUploadResponse multipartUploadResponse = createMultipartUpload(contentType);

            if (multipartUploadResponse.requestCharged().equals(RequestCharged.REQUESTER)) {
                // see https://docs.aws.amazon.com/en_us/amazonglacier/latest/dev/api-upload-part.html
                final String uploadId = multipartUploadResponse.uploadId();

                LOGGER.info("Successfully initiated multipart upload of \"{}\" with upload id: {}", fileName, uploadId);

                // For example, in the URL http://doc.s3.amazonaws.com/2006-03-01/AmazonS3.wsdl,
                // "doc" is the name of the bucket and "2006-03-01/AmazonS3.wsdl" is the key.
                final String key = remoteLocation + "/" + fileName;

                int offset = 0;
                for (int i = 1; offset < bytes.length; i++) {
                    // https://docs.aws.amazon.com/en_us/AmazonS3/latest/dev/llJavaUploadFile.html
                    // Because the last part could be less than 5 MB, adjust the part size as needed.
                    chunkSizeBytes = Math.min(chunkSizeBytes, (bytes.length - offset));

                    byte[] chunk = Arrays.copyOfRange(bytes, offset, chunkSizeBytes);

                    UploadPartRequest uploadRequest = UploadPartRequest.builder()
                            .bucket(bucket)
                            .key(key)
                            .uploadId(uploadId)
                            .partNumber(i)
                            .contentMD5(CryptoService.toMD5(chunk))
                            .build();

                    // Upload the part and add the response's ETag to our list.
                    UploadPartResponse uploadResult = s3Client.uploadPart(uploadRequest, RequestBody.fromBytes(bytes));

                    offset += chunkSizeBytes;
                }

                // Complete the multipart upload.
                CompleteMultipartUploadRequest compRequest = CompleteMultipartUploadRequest.builder()
                .bucket(bucket)
                .key(key)
                .uploadId(uploadId).build();
                s3Client.completeMultipartUpload(compRequest);

                return new ResponseEntity<>(
                        new UploadResultEntity(
                                UploadResultEntity.Result.SUCCESS,
                                fileName),
                        HttpStatus.OK
                );
            } else {
                LOGGER.error("Failed to get charge for multipart request [\"{}\", \"{}\"]", fileName, requestUUID);

                return new ResponseEntity<>(
                        new UploadResultEntity(
                                UploadResultEntity.Result.FAILURE,
                                fileName,
                                "Failed to get charge for multipart request"
                        ),
                        HttpStatus.UNAUTHORIZED
                );
            }
        } catch (SdkException | NoSuchAlgorithmException e) {
            LOGGER.error("Failed to upload file [\"{}\", \"{}\"]", fileName, requestUUID, e);

            return new ResponseEntity<>(
                    new UploadResultEntity(
                            UploadResultEntity.Result.FAILURE,
                            fileName,
                            "Failed to upload file \"" + fileName + "\":\n" + e.getMessage()
                    ),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Create multipart upload request.
     * @see <a href="https://docs.aws.amazon.com/en_us/AmazonS3/latest/dev/llJavaUploadFile.html">Upload a File</a>.
     *
     * @param contentType content type
     * @return {@link CreateMultipartUploadResponse} or <code>null</code>
     */
    private CreateMultipartUploadResponse createMultipartUpload(final String contentType) {
        return s3Client.createMultipartUpload(CreateMultipartUploadRequest.builder()
                .acl(ObjectCannedACL.BUCKET_OWNER_READ)
                .bucket(bucket)
                .contentType(contentType)
                .build());
    }
}
