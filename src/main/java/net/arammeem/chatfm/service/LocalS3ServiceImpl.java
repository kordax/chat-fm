/*
 * [LocalS3ServiceImpl.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import net.arammeem.chatfm.entity.rest.UploadResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * AWS S3 service implementation that uses REST template API with configured environment and remote URL.
 *
 */
@Service
public class LocalS3ServiceImpl implements S3Service {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalS3ServiceImpl.class);

    @Value("${fm.s3.connect-timeout-ms}")
    private int connectTimeout;
    @Value("${fm.s3.read-timeout-ms}")
    private int readTimeout;
    @Value("${fm.s3.remote-url}")
    private String remoteUrl;
    @Value("${fm.s3.remote-location}")
    private String remoteLocation;

    //PUT /db-backup.dat.gz HTTP/1.1
    //User-Agent: curl/7.15.5
    //Host: static.johnsmith.net:8080
    //Date: Tue, 27 Mar 2007 21:06:08 +0000
    //
    //x-amz-acl: public-read
    //content-type: application/x-download
    //Content-MD5: 4gJE4saaMU4BqNR0kLY+lw==
    //X-Amz-Meta-ReviewedBy: joe@johnsmith.net
    //X-Amz-Meta-ReviewedBy: jane@johnsmith.net
    //X-Amz-Meta-FileChecksum: 0x02661779
    //X-Amz-Meta-ChecksumAlgorithm: crc32
    //Content-Disposition: attachment; filename=database.dat
    //Content-Encoding: gzip
    //Content-Length: 5913339
    //
    //Authorization: AWS AKIAIOSFODNN7EXAMPLE:
    //ilyl83RwaSoYIEdixDQcA4OnAnc=

    private final RestTemplate template;

    @Autowired
    public LocalS3ServiceImpl(final RestTemplate template) {
        this.template = template;
    }

    /**
     * Upload file.
     * @see <a href="https://docs.aws.amazon.com/en_us/AmazonS3/latest/API/RESTObjectPUT.html">AWS PUT Object API reference</a>
     *
     * @param bytes bytes
     * @param fileName file name
     * @return HTTP response for client to send back
     */
    @Override
    public ResponseEntity<UploadResultEntity> uploadFile(final byte[] bytes, final String contentType, final String fileName) {
        LOGGER.info("Received request to upload file \"{}\"", fileName);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setContentLength(bytes.length);

        final HttpEntity<byte[]> request = new HttpEntity<>(bytes, headers);
        final String resultUrl = remoteUrl + "/" + remoteLocation + "/" + fileName;

        try {
            final ResponseEntity response = template.postForEntity(resultUrl, request, byte.class);

            if (response.getStatusCode().is2xxSuccessful()) {
                return new ResponseEntity<>(
                        new UploadResultEntity(
                                UploadResultEntity.Result.SUCCESS,
                                resultUrl,
                                fileName
                        ),
                        response.getHeaders(),
                        response.getStatusCode()
                );
            }
        } catch (RestClientException e) {
            LOGGER.error("Failure on file upload: {}", fileName, e);

            throw e;
        }

        return new ResponseEntity<>(
            new UploadResultEntity(
                    UploadResultEntity.Result.FAILURE,
                    resultUrl,
                    fileName
            ),
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Not implemented!
     *
     */
    @Override
    public ResponseEntity<UploadResultEntity> uploadFileAsMultipart(final byte[] bytes, final String contentType, final String fileName) {
        return new ResponseEntity<>(
                new UploadResultEntity(
                        UploadResultEntity.Result.FAILURE,
                        fileName,
                        "Multipart upload isn't implemented for local storage implementation"
                ),
                HttpStatus.NOT_IMPLEMENTED
        );
    }
}
