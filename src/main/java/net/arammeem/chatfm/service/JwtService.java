/*
 * [JwtService.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import com.google.gson.Gson;
import net.arammeem.chatfm.entity.jwt.JwtEntity;
import net.arammeem.chatfm.entity.jwt.JwtHeaderEntry;
import net.arammeem.chatfm.entity.jwt.JwtPayloadEntry;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

/**
 * JWT service.
 *
 */
@Service
public class JwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtService.class);

    private final CryptoService cryptoService;
    private final Gson gson = new Gson();

    private PrivateKey privateKey;
    private PublicKey publicKey;

    @Autowired
    public JwtService(final CryptoService cryptoService) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        this.cryptoService = cryptoService;

        final BufferedInputStream pvtBis = new BufferedInputStream(new FileInputStream("jwt/private_key.der"));
        final byte[] pvtBytes = pvtBis.readAllBytes();
        final BufferedInputStream pubBis = new BufferedInputStream(new FileInputStream("jwt/public_key.der"));
        final byte[] pubBytes = pubBis.readAllBytes();

        privateKey = cryptoService.readPrivateKey(pvtBytes);
        publicKey = cryptoService.readPublicKey(pubBytes);
    }

    /**
     * Parse encoded Jwt String (including dot delimeter character) and return resulting entry as [JwtEntity].
     *
     * @param jwt jwt-token
     * @throws IllegalArgumentException if provided jwt is illegal
     * @throws GeneralSecurityException on decryption error
     * @return jwt entry
     */
    public JwtEntity parseJwt(final String jwt) throws GeneralSecurityException {
        if (jwt == null || jwt.isEmpty()) throw new IllegalArgumentException(jwt);
        if (publicKey == null) throw new IllegalStateException("Failed to process public key file");

        // Substring 'Bearer: '
        // eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJhMDAwMDAwMC0wMDAwLTQwMDAtYTAwMC0wMDAwMDAwMDAwMDAifQ.LHZvgD_7BqsyyD1w5oYkC9FqYdXcCJUjJkZScBAkz80UViAPeAzv_FaMDM83ZOL4-yZ7H721vWOJHk_zCHudNIXGOSY_3asril9QNSgbVlNaBYUzavEseJQnsNDZsJ8Zi4M9vw595tFdRntnFx-rq4LxrjzUE65iPYidxQnEN5u9cj33ho8Vz2yerMD1ggjd4YSy7dxGQpSZTvTPPba2zS25zPccvgGX2dopjJPCgFq2i-a8kS_jon2fEn2Xmj-bMF2Okmt5uPO_K351UL9lIxrdQC5hB7sLcT5c5raE4i1VcXrtQzRd-u-XEz9sVzmeTipnODAOIIlDi4PrYfkAQw

        final String header;
        final String payload;
        final String signature;

        try {
            header = jwt.substring(0, jwt.indexOf('.'));
            payload = jwt.substring(jwt.indexOf('.') + 1, jwt.lastIndexOf('.'));
            signature = jwt.substring(jwt.lastIndexOf('.') + 1);
        } catch(IndexOutOfBoundsException e) {
            LOGGER.error("Failed to parse jwt token: {}", jwt, e);

            throw e;
        }

        if (header.isBlank()) {
            throw new IllegalArgumentException("Jwt header is not provided");
        }

        if (payload.isBlank()) {
            throw new IllegalArgumentException("Jwt payload is not provided");
        }

        if (signature.isBlank()) {
            throw new IllegalArgumentException("Jwt signature is not provided");
        }

        final Base64 base64 = new Base64(true);
        final JwtHeaderEntry headerEntry = gson.fromJson(new String(base64.decode(header)), JwtHeaderEntry.class);

        if (!headerEntry.getAlg().equals("RS256")) {
            throw new IllegalArgumentException("Unsupported alg provided: " + headerEntry.getAlg());
        }

        LOGGER.debug("Signature string: {}", signature);
        final byte[] signatureDec = base64.decode(signature);
        LOGGER.debug("Encrypted signature: {}", new String(signatureDec));

        final byte[] sigBytes = cryptoService.decrypt(signatureDec, publicKey);

        return new JwtEntity(headerEntry, gson.fromJson(new String(base64.decode(payload)), JwtPayloadEntry.class), new String(sigBytes));
    }

    /**
     * Generate encoded jwt token.
     *
     * @param jwtHeaderEntry jwt header
     * @param jwtPayloadEntry jwt payload
     */
    public String generateJwtToken(final JwtHeaderEntry jwtHeaderEntry, final JwtPayloadEntry jwtPayloadEntry) throws FileNotFoundException, GeneralSecurityException {
        if (privateKey == null) throw new FileNotFoundException("Failed to open private key content");

        final String headerStr = gson.toJson(jwtHeaderEntry);
        final String payloadStr = gson.toJson(jwtPayloadEntry);

        final String encHeaderStr = encodeJwtToBase64(headerStr.getBytes());
        final String encPayloadStr = encodeJwtToBase64(payloadStr.getBytes());

        final String signatureStr = encHeaderStr + "." + encPayloadStr;
        LOGGER.debug("Signature size: " + signatureStr.getBytes().length);

        final byte[] signatureRSA = cryptoService.encrypt(signatureStr.getBytes(), privateKey);
        LOGGER.debug("Encrypted signature: " + new String(signatureRSA));
        LOGGER.debug("Encrypted signature size: " + signatureRSA.length);

        return encHeaderStr + "." + encPayloadStr + "." + encodeJwtToBase64(signatureRSA);
    }

    /**
     * Encode Jwt bytes to Base64Url.
     *
     * @param jwtBytes jwt bytes
     * @return result string
     */
    private String encodeJwtToBase64(final byte[] jwtBytes) {
        return new Base64(true).encodeAsString(jwtBytes).replace("\r\n", "");
    }
}