/*
 * [FilePolicyManagementService.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * NOT IMPLEMENTED.
 *
 * TODO: Implement policy manager with Redis/Postgres stored state.
 */
@Service
public class FilePolicyManagementService {
    public Map<String, String> getPolicy(final String fileName) {
        return new HashMap<>(0);
    }
}
