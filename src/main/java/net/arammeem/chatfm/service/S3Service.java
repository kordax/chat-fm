/*
 * [S3Service.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import net.arammeem.chatfm.entity.rest.UploadResultEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * S3 Integration service.
 *
 */
@Component
public interface S3Service {
    /**
     * Upload file.
     *
     * @param bytes bytes
     * @param contentType content type
     * @param fileName file name
     * @return HTTP response for client to send back
     */
    ResponseEntity<UploadResultEntity> uploadFile(byte[] bytes, final String contentType, final String fileName);

    /**
     * Upload file as multipart.
     *
     * @param bytes bytes
     * @param contentType content type
     * @param fileName file name
     * @return HTTP response for client to send back
     */
    ResponseEntity<UploadResultEntity> uploadFileAsMultipart(byte[] bytes, final String contentType, final String fileName);
}
