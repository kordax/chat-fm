/*
 * [CryptoService.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;
import uk.co.lucasweb.aws.v4.signer.credentials.AwsCredentials;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * Cryptography service that provides crypto operations.
 *
 */
@SuppressWarnings("WeakerAccess")
@Service
public class CryptoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoService.class);

    @NotNull
    @Value("${fm.s3.auth.aws-access-key}")
    private String awsAccessKey;
    @NotNull
    @Value("${fm.s3.auth.aws-secret-access-key}")
    private String awsSecretAccessKey;

    private String alg = "RSA";
    private Cipher encCipher;
    private Cipher decCipher;

    private final DateFormat dateFormat = new SimpleDateFormat("YYYYMMDD'T'HHMMSS'Z'");

    public CryptoService() throws NoSuchAlgorithmException, NoSuchPaddingException {
        encCipher = Cipher.getInstance(alg);
        decCipher = Cipher.getInstance(alg);
    }

    public CryptoService(final String alg) throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.alg = alg;
        encCipher = Cipher.getInstance(alg);
        decCipher = Cipher.getInstance(alg);
    }

    /**
     * Encrypt data using private key.
     *
     * @param data data
     * @param privateKey private key
     * @throws InvalidKeyException exception for invalid key
     * @throws BadPaddingException exception on key parse error
     * @throws IllegalBlockSizeException exception on key parse error
     * @return encrypted data as String
     */
    public byte[] encrypt(final byte[] data, final PrivateKey privateKey) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        LOGGER.debug("Encrypting {} bytes", data.length);
        encCipher.init(Cipher.ENCRYPT_MODE, privateKey);

        return encCipher.doFinal(data);
    }

    /**
     * Decrypt data using public key.
     *
     * @param data data
     * @param publicKey public key
     * @throws InvalidKeyException exception for invalid key
     * @throws BadPaddingException exception on key parse error
     * @throws IllegalBlockSizeException exception on key parse error
     * @return decrypted data as String
     */
    public byte[] decrypt(final byte[] data, final PublicKey publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        LOGGER.debug("Decrypting {} bytes", data.length);
        decCipher.init(Cipher.DECRYPT_MODE, publicKey);

        return decCipher.doFinal(data);
    }

    /**
     * Read public key from file source bytes.
     *
     * @param source source bytes
     * @throws NoSuchAlgorithmException if used algorithm isn't supported
     * @throws InvalidKeySpecException exception for invalid key specification
     * @return public key instance
     */
    public PublicKey readPublicKey(final byte[] source) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return readPublicKey(source, alg);
    }

    /**
     * Read private key from file source bytes.
     *
     * @param source source bytes
     * @throws NoSuchAlgorithmException if used algorithm isn't supported
     * @throws InvalidKeySpecException exception for invalid key specification
     * @return private key instance
     */
    public PrivateKey readPrivateKey(final byte[] source) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return readPrivateKey(source, alg);
    }

    /**
     * Read public key from file source bytes.
     *
     * @param source source bytes
     * @param alg algorithm
     * @throws NoSuchAlgorithmException if used algorithm isn't supported
     * @throws InvalidKeySpecException exception for invalid key specification
     * @return public key instance
     */
    public PublicKey readPublicKey(final byte[] source, final String alg) throws NoSuchAlgorithmException, InvalidKeySpecException {
        final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(source);
        final KeyFactory kf = KeyFactory.getInstance(alg);

        LOGGER.debug("Reading public key");

        return kf.generatePublic(keySpec);
    }

    /**
     * Read private key from file source bytes.
     *
     * @param source source bytes
     * @param alg algorithm
     * @throws NoSuchAlgorithmException if used algorithm isn't supported
     * @throws InvalidKeySpecException exception for invalid key specification
     * @return private key instance
     */
    public PrivateKey readPrivateKey(final byte[] source, final String alg) throws NoSuchAlgorithmException, InvalidKeySpecException {
        final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(source);
        final KeyFactory kf = KeyFactory.getInstance(alg);

        LOGGER.debug("Reading private key");

        return kf.generatePrivate(keySpec);
    }

    /**
     * Generate AWS signature key.
     * @see <a href="https://docs.aws.amazon.com/en_us/general/latest/gr/signature-v4-examples.html">Deriving the Signing Key with Java</a>
     *
     * @param request HTTP request entity
     * @throws IllegalArgumentException if method call contains invalid arguments
     * @return AWS signature
     */
    public String getSignature(final HttpRequest request, final String contentInSha256) throws IllegalArgumentException {
        if (request == null) {
            LOGGER.error("Request was'nt provided");

            throw new IllegalArgumentException("Request was'nt provided");
        }

        List<String> hostHeader = request.getHeaders().get(HttpHeaders.HOST);
        if (hostHeader == null || hostHeader.isEmpty()) {
            LOGGER.error("\"{}\" was'nt provided in request", HttpHeaders.HOST);

            throw new IllegalArgumentException("\"" + HttpHeaders.HOST + "\" not provided in request\"");
        }

        HttpMethod requestMethod = request.getMethod();
        if (requestMethod == null) {
            LOGGER.error("Request method was'nt provided in request");

            throw new IllegalArgumentException("Request method was'nt provided in request");
        }

        final String headerHost = Objects.requireNonNull(request.getHeaders().get(HttpHeaders.HOST)).get(0);

        final Date date = new Date(System.currentTimeMillis());
        uk.co.lucasweb.aws.v4.signer.HttpRequest libRequestWrapper = new uk.co.lucasweb.aws.v4.signer.HttpRequest(request.getMethod().toString(), request.getURI());

        return uk.co.lucasweb.aws.v4.signer.Signer.builder()
                .awsCredentials(new AwsCredentials(awsAccessKey, awsSecretAccessKey))
                .header("Host", headerHost)
                .header("x-amz-date", dateFormat.format(date))
                .header("x-amz-content-sha256", contentInSha256)
                .buildS3(libRequestWrapper, contentInSha256)
                .getSignature();
    }

    public String getAlg() {
        return alg;
    }

    /**
     * Generate MD5 from byte array.
     *
     * @param bytes bytes
     * @return MD5 HEX string
     */
    public static String toMD5(final byte[] bytes) throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(bytes);
        final byte[] digest = md.digest();

        return DatatypeConverter.printHexBinary(digest);
    }
}