/*
 * [JwtEntity.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.entity.jwt;

/**
 * Jwt entry case class.
 *
 */
public class JwtEntity {
        private final JwtHeaderEntry jwtHeaderEntry;
        private final JwtPayloadEntry jwtPayloadEntry;
        private final String secret;

        /**
         * @param jwtHeaderEntry jwt header
         * @param jwtPayloadEntry jwt payload
         * @param secret RS256 secret key
         */
        public JwtEntity(final JwtHeaderEntry jwtHeaderEntry, final JwtPayloadEntry jwtPayloadEntry, final String secret) {
                this.jwtHeaderEntry = jwtHeaderEntry;
                this.jwtPayloadEntry = jwtPayloadEntry;
                this.secret = secret;
        }

        public JwtHeaderEntry getJwtHeaderEntry() {
                return jwtHeaderEntry;
        }

        public JwtPayloadEntry getJwtPayloadEntry() {
                return jwtPayloadEntry;
        }

        public String getSecret() {
                return secret;
        }

        @Override
        public String toString() {
                return "JwtEntity{" +
                        "jwtHeaderEntry=" + jwtHeaderEntry +
                        ", jwtPayloadEntry=" + jwtPayloadEntry +
                        ", secret='" + secret + '\'' +
                        '}';
        }
}