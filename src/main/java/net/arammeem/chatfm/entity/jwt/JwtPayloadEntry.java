/*
 * [JwtPayloadEntry.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.entity.jwt;

import com.google.gson.annotations.SerializedName;

/**
 * Jwt payload entry case class.
 *
 */
public class JwtPayloadEntry {
    @SerializedName("name")
    private final String name;
    @SerializedName("email")
    private final String email;
    @SerializedName("userId")
    private final String userId;

    /**
     * @param name user userId UUID value
     * @param email email
     * @param userId user UUID
     */
    public JwtPayloadEntry(final String name, final String email, final String userId) {
        this.name = name;
        this.email = email;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "JwtPayloadEntry{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
