/*
 * [JwtHeaderEntry.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.entity.jwt;

import com.google.gson.annotations.SerializedName;

/**
 * Jwt header entry case class.
 *
 */
public class JwtHeaderEntry {
    @SerializedName("alg")
    private final String alg;
    @SerializedName("typ")
    private final String typ;

    /**
     * @param alg alg value
     * @param typ typ value
     */
    public JwtHeaderEntry(final String alg, final String typ) {
        this.alg = alg;
        this.typ = typ;
    }

    public String getAlg() {
        return alg;
    }

    public String getTyp() {
        return typ;
    }

    @Override
    public String toString() {
        return "JwtHeaderEntry{" +
                "alg='" + alg + '\'' +
                ", typ='" + typ + '\'' +
                '}';
    }
}