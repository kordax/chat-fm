/*
 * [UploadResultEntity.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.entity.rest;

/**
 * File upload result REST entity.
 *
 */
public class UploadResultEntity {
    public enum Result {
        SUCCESS,
        FAILURE
    }

    private final Result result;
    private final String fileName;
    private String message;
    private String url;

    public UploadResultEntity(final Result result, final String fileName) {
        this.result = result;
        this.fileName = fileName;
    }

    public UploadResultEntity(final Result result, final String fileName, final String message) {
        this.result = result;
        this.fileName = fileName;
        this.message = message;
    }

    public UploadResultEntity(final Result result, final String fileName, final String message, final String url) {
        this.result = result;
        this.fileName = fileName;
        this.message = message;
        this.url = url;
    }

    public Result getResult() {
        return result;
    }

    public String getUrl() {
        return url;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMessage() {
        return message;
    }
}
