/*
 * [S3ServiceConfig.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.config;

import net.arammeem.chatfm.service.AWSS3ServiceImpl;
import net.arammeem.chatfm.service.FilePolicyManagementService;
import net.arammeem.chatfm.service.S3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class S3ServiceConfig {
    @Bean
    @Autowired
    public S3Service s3Service(final S3Client s3Client, final FilePolicyManagementService filePolicyManagementService, final RestTemplate restTemplate) {
        return new AWSS3ServiceImpl(s3Client, filePolicyManagementService);
    }
}
