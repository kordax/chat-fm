/*
 * [ApplicationPropertiesConfig.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Configuration
@EnableConfigurationProperties(FmProperties.class)
@Validated
public class FmPropertiesConfig {
    @Valid
    @NotNull
    private FmProperties fmProperties;

    public FmProperties getFmProperties() {
        return fmProperties;
    }
}
