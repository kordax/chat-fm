/*
 * [ApplicationProperties.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
@ConfigurationProperties(prefix = "fm")
@Validated
@SuppressWarnings({"unused"})
public class FmProperties {
    @Valid
    private S3 s3;

    public S3 getS3() {
        return s3;
    }

    public void setS3(S3 s3) {
        this.s3 = s3;
    }

    public static class S3 {
        @NotBlank
        private String bucket;
        @NotBlank
        private String remoteUrl;
        @NotBlank
        private String remoteLocation;
        @Positive
        private int connectTimeoutMs;
        @Positive
        private int readTimeoutMs;

        @Valid
        @NotNull
        private Auth auth;

        public static class Auth {
            private String awsAccessKey;
            private String awsSecretAccessKey;
            private String awsSessionToken;
            @NotBlank
            private String ownerId;
            @NotBlank
            private String ownerDisplayName;

            public String getAwsAccessKey() {
                return awsAccessKey;
            }

            public void setAwsAccessKey(final String awsAccessKey) {
                this.awsAccessKey = awsAccessKey;
            }

            public String getAwsSecretAccessKey() {
                return awsSecretAccessKey;
            }

            public void setAwsSecretAccessKey(final String awsSecretAccessKey) {
                this.awsSecretAccessKey = awsSecretAccessKey;
            }

            public String getAwsSessionToken() {
                return awsSessionToken;
            }

            public void setAwsSessionToken(String awsSessionToken) {
                this.awsSessionToken = awsSessionToken;
            }

            public String getOwnerId() {
                return ownerId;
            }

            public void setOwnerId(final String ownerId) {
                this.ownerId = ownerId;
            }

            public String getOwnerDisplayName() {
                return ownerDisplayName;
            }

            public void setOwnerDisplayName(final String ownerDisplayName) {
                this.ownerDisplayName = ownerDisplayName;
            }
        }

        public static class Upload {
            @Positive
            private int chunkSizeBytes;

            public int getChunkSizeBytes() {
                return chunkSizeBytes;
            }

            public void setChunkSizeBytes(final int chunkSizeBytes) {
                this.chunkSizeBytes = chunkSizeBytes;
            }
        }

        public String getBucket() {
            return bucket;
        }

        public void setBucket(final String bucket) {
            this.bucket = bucket;
        }

        public String getRemoteUrl() {
            return remoteUrl;
        }

        public void setRemoteUrl(final String remoteUrl) {
            this.remoteUrl = remoteUrl;
        }

        public String getRemoteLocation() {
            return remoteLocation;
        }

        public void setRemoteLocation(final String remoteLocation) {
            this.remoteLocation = remoteLocation;
        }

        public int getConnectTimeoutMs() {
            return connectTimeoutMs;
        }

        public void setConnectTimeoutMs(final int connectTimeoutMs) {
            this.connectTimeoutMs = connectTimeoutMs;
        }

        public int getReadTimeoutMs() {
            return readTimeoutMs;
        }

        public void setReadTimeoutMs(final int readTimeoutMs) {
            this.readTimeoutMs = readTimeoutMs;
        }

        public Auth getAuth() {
            return auth;
        }

        public void setAuth(final Auth auth) {
            this.auth = auth;
        }
    }
}
