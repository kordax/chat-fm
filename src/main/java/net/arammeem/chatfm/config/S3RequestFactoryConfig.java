/*
 * [S3RequestFactoryConfig.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.config;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Request factory configuration.
 *
 */
@Configuration
public class S3RequestFactoryConfig {
    @Value("${fm.s3.connect-timeout-ms}")
    private int connectTimeout;

    @Value("${fm.s3.read-timeout-ms}")
    private int readTimeout;

    @Bean
    public ClientHttpRequestFactory requestFactory() {
        final CloseableHttpClient httpClient = HttpClientBuilder.create().disableAutomaticRetries().build();
        final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);

        // read timeout
        if(!StringUtils.isEmpty(readTimeout)) {
            factory.setReadTimeout(readTimeout * 1000);
        }

        // connect timeout
        if(!StringUtils.isEmpty(connectTimeout)) {
            factory.setConnectTimeout(connectTimeout * 1000);
        }

        return factory;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(requestFactory());
    }
}
