/*
 * [S3ClientConfig.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.S3Configuration;

/**
 * S3 client configuration.
 *
 */
@Configuration
public class S3ClientConfig {
    @Value("${fm.s3.accelerate-mode}")
    private boolean accelerateMode;
    @Value("${fm.s3.auth.aws-access-key}")
    private String awsAccessKey;
    @Value("${fm.s3.auth.aws-secret-access-key}")
    private String awsSecretAccessKey;
    @Value("${fm.s3.auth.aws-session-token}")
    private String awsSessionToken;

    @Bean
    public S3Client s3Client() {
        S3ClientBuilder clientBuilder = S3Client.builder()
                .credentialsProvider(credentialsProvider())
                .region(Region.EU_WEST_1)
                .serviceConfiguration(S3Configuration.builder().accelerateModeEnabled(accelerateMode).build());

        return clientBuilder.build();
    }

    private AwsCredentialsProvider credentialsProvider() {
        if (awsAccessKey.isBlank() || awsSecretAccessKey.isBlank() || awsSessionToken.isBlank()) {
            AwsCredentialsProvider credentialsProvider = DefaultCredentialsProvider.create();

            awsAccessKey = credentialsProvider.resolveCredentials().accessKeyId();
            awsSecretAccessKey = credentialsProvider.resolveCredentials().secretAccessKey();
        }

        return () -> AwsSessionCredentials.create(awsAccessKey, awsSecretAccessKey, awsSessionToken);
    }
}
