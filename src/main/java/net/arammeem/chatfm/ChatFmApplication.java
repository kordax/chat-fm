/*
 * [ChatFmApplication.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class with Spring application method.
 *
 */
@SpringBootApplication
public class ChatFmApplication {
    /**
     * Start Spring application.
     *
     * @param args application arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(ChatFmApplication.class, args);
    }
}
