/*
 * [UploadController.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.controller;

import net.arammeem.chatfm.entity.jwt.JwtEntity;
import net.arammeem.chatfm.entity.rest.UploadResultEntity;
import net.arammeem.chatfm.service.JwtService;
import net.arammeem.chatfm.service.S3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * REST controller that's responsible for file upload to S3 storage.
 *
 */
@RestController
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

    private final JwtService jwtService;
    private final S3Service s3Service;

    @Autowired
    public UploadController(final JwtService jwtService, final S3Service s3Service) {
        this.jwtService = jwtService;
        this.s3Service = s3Service;
    }

    /**
     * Proxy uploaded file to S3 storage.
     *
     * @param payload payload
     */
    // Is it possible to use redirect here?
    @PostMapping("/upload")
    public ResponseEntity<UploadResultEntity> processPayload(
            @RequestParam("payload") final MultipartFile payload,
            @RequestParam("token") final String token
    ) {
        final byte[] bytes;
        final JwtEntity parsedJwtEntity;

        try {
            parsedJwtEntity = jwtService.parseJwt(token);
        } catch (GeneralSecurityException e) {
            LOGGER.error("Failed to parse JWT token", e);

            return new ResponseEntity<>(new UploadResultEntity(UploadResultEntity.Result.FAILURE, payload.getOriginalFilename(), "Failed to parse JWT token"), HttpStatus.UNAUTHORIZED);
        }

        LOGGER.info("Received request to upload file from: " + parsedJwtEntity.getJwtPayloadEntry().toString());

        try {
            bytes = payload.getBytes();
        } catch (IOException e) {
            LOGGER.error("Failed to retrieve file bytes", e);

            return new ResponseEntity<>(new UploadResultEntity(UploadResultEntity.Result.FAILURE, payload.getOriginalFilename(), "Failed to retrieve file bytes"), HttpStatus.UNAUTHORIZED);
        }

        try {
            return s3Service.uploadFile(bytes, payload.getContentType(), payload.getOriginalFilename());
        } catch (RestClientException e) {
            return new ResponseEntity<>(
                    new UploadResultEntity(UploadResultEntity.Result.FAILURE, payload.getOriginalFilename(), "Failed to retrieve file bytes"),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }
}
