/*
 * [JwtServiceTests.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 11 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.tests;

import net.arammeem.chatfm.entity.jwt.JwtHeaderEntry;
import net.arammeem.chatfm.entity.jwt.JwtPayloadEntry;
import net.arammeem.chatfm.service.CryptoService;
import net.arammeem.chatfm.service.JwtService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.NoSuchPaddingException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Crypto service tests.
 *
 */
@ExtendWith(MockitoExtension.class)
class JwtServiceTests {
    private static final Logger Logger = LoggerFactory.getLogger(JwtServiceTests.class);

    private final CryptoService cryptoService = new CryptoService();
    private final JwtService jwtService = new JwtService(cryptoService);

    JwtServiceTests() throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, InvalidKeySpecException { }

    /**
     * Generate and parse JWT token.
     *
     * @case positive: valid parameters
     * @result we should generate and parse JWT token w/o exceptions
     */
    @Test
    void generateAndParseJwtToken_When_ValidParametersProvided_Success() {
        final String headerAlg = "RS256";
        final String headerTyp = "JWT";
        final String payloadName = "Dmitry Morozov";
        final String payloadEmail = "d.morozov@redmadrobot.com";
        final String payloadUserId = UUID.randomUUID().toString();

        JwtHeaderEntry jwtHeaderEntry = new JwtHeaderEntry(
                headerAlg,
                headerTyp
        );

        JwtPayloadEntry jwtPayloadEntry = new JwtPayloadEntry(
                payloadName,
                payloadEmail,
                payloadUserId
        );

        final String jwtToken;

        try {
            jwtToken = jwtService.generateJwtToken(jwtHeaderEntry, jwtPayloadEntry);

            Assertions.assertNotNull(jwtToken);
            Assertions.assertFalse(jwtToken.isBlank());

            Assertions.assertDoesNotThrow(() -> jwtService.parseJwt(jwtToken));
        } catch (FileNotFoundException | GeneralSecurityException e) {
            Logger.error("Failed to generate jwt token", e);

            fail(e);
        }
    }

    /**
     * Generate and parse JWT token.
     *
     * @case positive: valid parameters
     * @result we should generate and parse JWT token w/o exceptions
     */
    @Test
    void generateJwtToken_When_InvalidHeaderAlgProvided_Failure() {
        final String headerAlg = "RS256";
        final String headerTyp = "JWT";
        final String payloadName = "Dmitry Morozov";
        final String payloadEmail = "d.morozov@redmadrobot.com";
        final String payloadUserId = UUID.randomUUID().toString();

        JwtHeaderEntry jwtHeaderEntry = new JwtHeaderEntry(
                headerAlg,
                headerTyp
        );

        JwtPayloadEntry jwtPayloadEntry = new JwtPayloadEntry(
                payloadName,
                payloadEmail,
                payloadUserId
        );

        final String jwtToken;

        try {
            jwtToken = jwtService.generateJwtToken(jwtHeaderEntry, jwtPayloadEntry);

            Assertions.assertNotNull(jwtToken);
            Assertions.assertFalse(jwtToken.isBlank());

            Assertions.assertDoesNotThrow(() -> jwtService.parseJwt(jwtToken));
        } catch (FileNotFoundException | GeneralSecurityException e) {
            Logger.error("Failed to generate jwt token", e);

            fail(e);
        }
    }
}
