/*
 * [ChatFmApplicationTests.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 07 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.tests;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

/**
 * Main tests class that contains all test suites.
 *
 */
@SuppressWarnings("JUnit5Platform")
@SuiteDisplayName("File manager unit tests suite")
@RunWith(JUnitPlatform.class)
@SelectClasses({CryptoServiceTests.class, JwtServiceTests.class})
public class ChatFmApplicationTests { }
