/*
 * [CryptoServiceTests.java]
 * [chat-fm]
 *
 * Created by [Dmitry Morozov] on 11 December 2018.
 * Copyright © 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatfm.tests;

import net.arammeem.chatfm.service.CryptoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Crypto service tests.
 *
 */
class CryptoServiceTests {
    private static final Logger Logger = LoggerFactory.getLogger(CryptoServiceTests.class);

    private final CryptoService cryptoService = new CryptoService();
    private PrivateKey privateKey;
    private PublicKey publicKey;

    CryptoServiceTests() throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final BufferedInputStream pvtBis = new BufferedInputStream(new FileInputStream("jwt/private_key.der"));
        final byte[] pvtBytes = pvtBis.readAllBytes();
        final BufferedInputStream pubBis = new BufferedInputStream(new FileInputStream("jwt/public_key.der"));
        final byte[] pubBytes = pubBis.readAllBytes();

        privateKey = cryptoService.readPrivateKey(pvtBytes);
        publicKey = cryptoService.readPublicKey(pubBytes);
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case positive: standard algorithm is selected and 245 characters provided
     * @result we should encrypt and decrypt contents without exception
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd245CharsProvided_Success() {
        final String contentsStr = "z".repeat(245);
        final byte[] contents = contentsStr.getBytes();
        byte[] encryptedData = null;

        try {
            encryptedData = cryptoService.encrypt(contents, privateKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        byte[] decryptedData = null;

        try {
            decryptedData = cryptoService.decrypt(encryptedData, publicKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        Assertions.assertArrayEquals(contents, decryptedData);
        Assertions.assertEquals(contentsStr, new String(decryptedData));
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case positive: standard algorithm is selected and 1 character provided
     * @result we should encrypt and decrypt contents without exception
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd1CharProvided_Success() {
        final String contentsStr = "z".repeat(1);
        final byte[] contents = contentsStr.getBytes();
        byte[] encryptedData = null;

        try {
            encryptedData = cryptoService.encrypt(contents, privateKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        byte[] decryptedData = null;

        try {
            decryptedData = cryptoService.decrypt(encryptedData, publicKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        Assertions.assertArrayEquals(contents, decryptedData);
        Assertions.assertEquals(contentsStr, new String(decryptedData));
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case positive: standard algorithm is selected and 0 characters were provided
     * @result we should encrypt and decrypt contents without exception
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd0CharsProvided_Success() {
        final String contentsStr = "z".repeat(0);
        final byte[] contents = contentsStr.getBytes();
        byte[] encryptedData = null;

        try {
            encryptedData = cryptoService.encrypt(contents, privateKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        byte[] decryptedData = null;

        try {
            decryptedData = cryptoService.decrypt(encryptedData, publicKey);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.error("Failed to encrypt contents", e);

            fail(e);
        }

        Assertions.assertArrayEquals(contents, decryptedData);
        Assertions.assertEquals(contentsStr, new String(decryptedData));
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case negative: standard algorithm is selected and 246 characters were provided
     * @result we should catch {@link IllegalBlockSizeException}
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd246CharsProvided_Failure() {
        final String contentsStr = "z".repeat(246);
        final byte[] contents = contentsStr.getBytes();

        Assertions.assertThrows(IllegalBlockSizeException.class, () -> cryptoService.encrypt(contents, privateKey));
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case negative: standard algorithm is selected and 490 characters were provided
     * @result we should catch {@link IllegalBlockSizeException}
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd490CharsProvided_Failure() {
        final String contentsStr = "z".repeat(490);
        final byte[] contents = contentsStr.getBytes();

        Assertions.assertThrows(IllegalBlockSizeException.class, () -> cryptoService.encrypt(contents, privateKey));
    }

    /**
     * Encrypt and decrypt contents with private/public keys.
     *
     * @case negative: standard algorithm is selected and 100000 characters were provided
     * @result we should catch {@link IllegalBlockSizeException}
     */
    @Test
    void encryptAndDecryptSameData_When_StdAlgIsSelectedAnd100000CharsProvided_Failure() {
        final String contentsStr = "z".repeat(100000);
        final byte[] contents = contentsStr.getBytes();

        Assertions.assertThrows(IllegalBlockSizeException.class, () -> cryptoService.encrypt(contents, privateKey));
    }
}
